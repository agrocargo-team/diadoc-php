<?php

namespace AgroCargo\Diadoc\Exception;

class DiadocApiNotPaidException extends DiadocApiException
{
	protected string $token;

	public function getToken(): string
	{
		return $this->token;
	}

	public function setToken(string $token): void
	{
		$this->token = $token;
	}
}
