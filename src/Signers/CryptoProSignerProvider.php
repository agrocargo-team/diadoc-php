<?php

namespace AgroCargo\Diadoc\Signers;

use Symfony\Component\Process\Process;

class CryptoProSignerProvider implements SignerProviderInterface
{
	private string $certThumbprint;

	private ?string $pin = null;

	public function __construct(string $certThumbprint, ?string $pin = null)
	{
		$this->certThumbprint = $certThumbprint;
		$this->pin = $pin;
	}

	private function getCryptoProProcess(array $args = [], $input = null): Process
	{
		array_unshift($args, 'cryptcp');
		return new Process($args);
	}

	public function encrypt(string $plainData): string
	{
		// TODO: Implement encrypt() method.
	}

	public function decrypt(string $encryptedData): string
	{
		// TODO: Implement decrypt() method.
	}

	public function sign(string $data): string
	{
		$tempInputFile = tmpfile();
		$tempOutputFile = tmpfile();
		$tempInputFileMeta = stream_get_meta_data($tempInputFile);
		$tempOutputFileMeta = stream_get_meta_data($tempOutputFile);

		fwrite($tempInputFile, $data);
		$process = $this->getCryptoProProcess([
			'-sign',
			'-der',
			'-pin', $this->pin,
			'-thumbprint', $this->certThumbprint,
			$tempInputFileMeta['uri'],
			$tempOutputFileMeta['uri'],
		]);
		$process->mustRun();
		fseek($tempOutputFile, 0);
		$sign = stream_get_contents($tempOutputFile);

		fclose($tempInputFile);
		fclose($tempOutputFile);
		return $sign;
	}

	public function checkSign(string $data, string $sign): bool
	{
		// TODO: Implement checkSign() method.
	}
}
