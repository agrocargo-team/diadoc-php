<?php

namespace AgroCargo\Diadoc\Signers;

use AgroCargo\Diadoc\Api\Proto\Content_v3;
use AgroCargo\Diadoc\Api\Proto\Dss\DssOperationStatus;
use AgroCargo\Diadoc\Api\Proto\Dss\DssSignFile;
use AgroCargo\Diadoc\Api\Proto\Dss\DssSignRequest;
use AgroCargo\Diadoc\DiadocApi;
use Carbon\Carbon;

class DssCertificateSignerProvider implements SignerProviderInterface
{
	private const WAIT_TIMEOUT_SEC = 60 * 5;

	private const WAIT_SEC = 15;

	private DiadocApi $diadocApi;

	private string $boxId;

	private string $certThumbprint;

	public function __construct(DiadocApi $diadocApi, string $boxId, string $certThumbprint)
	{
		$this->diadocApi = $diadocApi;
		$this->boxId = $boxId;
		$this->certThumbprint = $certThumbprint;
	}

	public function encrypt(string $plainData): string
	{
		throw new SignerProviderException('Not supported');
	}

	public function decrypt(string $encryptedData): string
	{
		throw new SignerProviderException('Not supported');
	}

	public function sign(string $data): string
	{
		$nameOnShelf = $this->diadocApi->shelfUpload($data);

		$content = new Content_v3();
		$content->setNameOnShelf($nameOnShelf);

		$file = new DssSignFile();
		$file->setFileName('test.txt');
		$file->setContent($content);

		$request = new DssSignRequest();
		$request->addFiles($file);

		$operation = $this->diadocApi->dssSign(
			$request,
			$this->boxId,
			$this->certThumbprint
		);
		$startedAt = Carbon::now();
		while (true) {
			$result = $this->diadocApi->dssSignResult($this->boxId, $operation->getTaskId());
			switch ($result->getOperationStatus()->value()) {
				case DssOperationStatus::Completed_VALUE:
					$signingResult = $result->getFileSigningResultsList()[0];
					return $signingResult->getSignature();
				case DssOperationStatus::InProgress_VALUE:
					if (Carbon::now()->diffInSeconds($startedAt, true) > self::WAIT_TIMEOUT_SEC)
						throw new SignerProviderException('Timeout reached');
					sleep(self::WAIT_SEC);
					break;
				default:
					throw new SignerProviderException(sprintf('Signing has failed: %s', $result->getOperationStatus()->name()));
			}
		}
	}

	public function checkSign(string $data, string $sign): bool
	{
		throw new SignerProviderException('Not supported');
	}
}
