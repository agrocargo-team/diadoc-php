<?php

namespace AgroCargo\Diadoc;

use AgroCargo\Diadoc\Api\OpenId\TokenResponse;
use AgroCargo\Diadoc\Exception\DiadocApiException;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\RequestOptions;
use Illuminate\Support\Str;

class DiadocOpenIdApi
{
	const METHOD_GET = 'GET';
	const METHOD_POST = 'POST';

	const RESOURCE_TOKEN = '/token';

	private string $serviceUrl;

	private string $clientId;

	private string $clientSecret;

	public function __construct(string $serviceUrl, string $clientId, string $clientSecret)
	{
		$this->serviceUrl = $serviceUrl;
		$this->clientId = $clientId;
		$this->clientSecret = $clientSecret;
	}

	public function getAuthUrl(string $redirectUrl): string
	{
		$scope = ['openid', 'email', 'phone', 'offline_access'];

		$params = [
			'response_type' => 'code',
			'scope' => implode(' ', $scope),
			'client_id' => $this->clientId,
			'redirect_uri' => $redirectUrl,
			'state' => Str::random(),
			'nonce' => Str::random(),
		];

		$query = http_build_query($params);

		return sprintf('%s/authorize?%s', $this->getServiceUrl(), $query);
	}

	public function getToken(string $code): TokenResponse
	{
		$response = $this->doRequest(self::RESOURCE_TOKEN, [], self::METHOD_POST, [
			'grant_type' => 'authorization_code',
			'authorization_code' => $code,
			'client_id' => $this->clientId,
			'client_secret' => $this->clientSecret,
			'redirect_uri' => 'https://example.com',
		]);

		$json = json_decode($response, true);

		return (new TokenResponse)
			->setIdToken($json['id_token'])
			->setAccessToken($json['access_token'])
			->setRefreshToken($json['refresh_token'])
			->setExpiresIn($json['expires_in']);
	}

	public function refreshToken(string $refreshToken): TokenResponse
	{
		$response = $this->doRequest(self::RESOURCE_TOKEN, [], self::METHOD_POST, [
			'grant_type' => 'refresh_token',
			'client_id' => $this->clientId,
			'client_secret' => $this->clientSecret,
			'refresh_token' => $refreshToken,
		]);

		$json = json_decode($response, true);

		return (new TokenResponse)
			->setAccessToken($json['access_token'])
			->setRefreshToken($json['refresh_token'])
			->setExpiresIn($json['expires_in']);
	}

	protected function doRequest($resource, $params = [], $method = self::METHOD_GET, $data = ''): string
	{
		try {
			$client = new Client([
				'base_uri' => $this->getServiceUrl(),
			]);

			$requestOptions = [
				RequestOptions::HEADERS => [],
				RequestOptions::QUERY => $params,
				RequestOptions::BODY => $data,
			];

			$response = $client->request(
				$method,
				$resource,
				$requestOptions,
			);

			return $response->getBody()->getContents();
		} catch (GuzzleException $guzzleException) {
			throw new DiadocApiException($guzzleException->getMessage());
		}
	}

	public function getServiceUrl(): string
	{
		$url = $this->serviceUrl;
		if (!Str::startsWith($url, ['http://', 'https://'])) {
			$url = 'https://' . $url;
		}
		if (Str::endsWith($url, '/')) {
			$url = mb_substr($url, 0, -1);
		}
		return $url;
	}
}
