<?php

namespace AgroCargo\Diadoc\Api\OpenId;

class TokenResponse
{
	private string $idToken;

	private string $accessToken;

	private string $refreshToken;

	private int $expiresIn;

	public function getIdToken(): string
	{
		return $this->idToken;
	}

	public function setIdToken(string $idToken): TokenResponse
	{
		$this->idToken = $idToken;
		return $this;
	}

	public function getAccessToken(): string
	{
		return $this->accessToken;
	}

	public function setAccessToken(string $accessToken): TokenResponse
	{
		$this->accessToken = $accessToken;
		return $this;
	}

	public function getRefreshToken(): string
	{
		return $this->refreshToken;
	}

	public function setRefreshToken(string $refreshToken): TokenResponse
	{
		$this->refreshToken = $refreshToken;
		return $this;
	}

	public function getExpiresIn(): int
	{
		return $this->expiresIn;
	}

	public function setExpiresIn(int $expiresIn): TokenResponse
	{
		$this->expiresIn = $expiresIn;
		return $this;
	}
}
